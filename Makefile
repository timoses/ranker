export PATH_TO_DATAP4K=/mnt/c/games_rsi/StarCitizen/LIVE/Data.p4k

.PHONY: build_and_deploy
build_and_deploy: build deploy

.PHONY: build
build: src/assets/keys_differ.html
	npm install
	npm run build

.PHONY: deploy
deploy:
	./util/deploy.sh

venv: venv3

venv3:
	python3 -m virtualenv venv3
	(. venv3/bin/activate ; pip install yq)

.PHONY: extract-sc-data
extract-sc-data: venv
	(. venv3/bin/activate ; ./util/sc_extract.sh)


src/assets/keys_differ.html: src/assets/keys_* util/keys_differ.sh
	./util/keys_differ.sh > public/keys_differ.html
