# Galactic Hike


<!-- vim-markdown-toc GFM -->

* [Chart](#chart)
* [Technical stuff](#technical-stuff)
    * [Technologies](#technologies)
    * [Build & Deploy](#build--deploy)
    * [Develop](#develop)
* [Licensing](#licensing)

<!-- vim-markdown-toc -->

## Chart

The chart uses the [ranker.js](src/assets/ranker.js) library.
See [docs/ranker.md](./docs/ranker.md)


## Technical stuff

Requirements:
* npm

### Technologies

This project uses the [Vue.js](https://vuejs.org/) framework.

To gather intel from Star Citizen we use [unp4k](https://github.com/dolkensp/unp4k).
See also [util/README.md](./util/README.md)


### Build & Deploy

```
npm install
npm run build
```

Afterwards the `dist` directory can be moved to a web server.

### Develop

```
npm install
npm run dev
```

## Licensing

* Website under CC BY-SA
* Repo under GPLv3

See https://creativecommons.org/share-your-work/licensing-considerations/compatible-licenses
