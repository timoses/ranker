# Ranker
## Algorithm

Two aspects control the generation of the chart:
- **Units**
- **Ranks**

**Units** define different areas of expertise in an organization.
They may have children to denote relations to one another.

Example:
```
units:
  mgmt:
    name: Management
    children:
      - cook
      - server
  cook:
    name: Cook
  server:
    name: Server
```

**Ranks** are represented in an ordered list.
Ranks also define which units they may appear in.

Example:
```
ranks:
  - name: Owner
    id: owner
    units:
      - mgmt
  - name: Chef
    id: chef
    units:
      - mgmt
      - cook
  - name: Worker
    id: worker
    units:
      - cook
      - server
```

*Ranker* will generate a list of possible combinations of unit groups based on the `units` definition.
The order is reflected in the units' children relations.

Example:
```
- mgmt
- mgmt, cook
- mgmt, server
- cook
- server
```

From this the chart ranking is built up in the following steps:
1. Each available rank is sorted into the unit groups if that rank is available in that unit group (defined in `ranks`).
    ```
    - mgmt:         Owner <- Chef
    - mgmt, cook:   Owner <- Chef <- Worker
    - mgmt, server: Owner <- Worker
    - cook:         Chef <- Worker
    - server:       Worker
    ```
2. Now each highest rank in a unit group is linked to the lowest rank in the next highest group resulting in:
    ```
    Owner(mgmt) <- Chef(mgmt)
        <- Owner(mgmt,cook) <- Chef(mgmt,cook) <- Worker(mgmt,cook)
        <- Owner(mgmt,server) <- Worker(mgmt,server)
        <- Chef(cook) <- Worker(cook)
        <- Worker(server
    ```

This gives us a directed graph with nodes of the form `Rank(Unit Group)`.

Let's say we have 4 people working with the following roles (Unit, Rank):
```
- Andrew: Management, Owner
- Irene: Cook, Chef
- Sebastian: Cook, Worker
- Bella: Server, Worker
```

Each is sorted into the node matching their unit group and rank.

In the last step we will discard all nodes that have no members and then resolve the correct head for each node based on the following strategy:

For each node we check the following conditions, and if they do not match check the next higher node until we find a matching parent:
* The parent must be occupied
* The parent must be of higher rank
* The parent is in the same unit group or all of our units in our unit group are descendants of the parent's units' children.

This way we should end up with the following:
```
                     Andrew (Rank: Owner, Unit: Management)
                     │                   │
                     │                   │
                     ▼                   ▼
Bella (Rank: Worker, Unit: Server)    Irene (Rank: Chef, Unit: Cook)
                                         │
                                         │
                                         ▼
                                      Sebastian (Rank: Worker, Unit: Cook)
```



## Issues

Don't be a wanker who rhymes with ranker. If you've got issues create an [Issue](https://codeberg.org/timoses/ranker/issues). You won't even need a tissue.
