import cloneDeep from 'clone-deep';

function sanity_check(units, ranks) {
    units.forEach(unit => {
        if (!('children' in unit)) { return; }
        unit.children.forEach(child_id => {
            if (!units.some(ounit => ounit.id == child_id)) {
                throw new Error(`Units misconfigured: Could not find unit with id '${child_id}' defined in unit '${unit.id}.'`)
            }
        })
        console.log('unit', unit)
        if (!(ranks.some(rank => rank.units.some(runit_id => unit.id == runit_id)))) {
            throw new Error(`Unit with id '${unit.id}' is not assigned to any rank.`)
        }
    })

    ranks.forEach(rank => {
        if (!('units' in rank)) {
            throw new Error(`Rank '${rank.id}' is missing a 'units' configuration.`)
        }
        rank.units.forEach(unit_id => {
            if (!units.some(ounit => unit_id == ounit.id)) {
                throw new Error(`Rank '${rank.id}' defines a child with id '${unit_id}' which does not exist in units.`)
            }
        })
    })
}

export function generate(units, ranks, members) {

        sanity_check(units, ranks)

        // Holds nodes of the modelled graph in an
        // object with unit goups as keys (model[unit_group][])
        var model = {}

        // 1. For every user create a node for its rank within a units combination list

            var unit_groups = []
            units.forEach(unit => {
                var cur_groups = []
                cur_groups.push([unit.id])

                function traverse_children(units, keys) {
                    var cur_key = keys[ keys.length - 1 ]
                    var cur_unit = units.find(unit => unit.id == cur_key)
                    if (!('children' in cur_unit)) { return; }
                    cur_unit.children.forEach(child => {
                        cur_groups.push(keys.concat([child]))
                    })
                    cur_unit.children.forEach(child => {
                        traverse_children(units, keys.concat([child]))
                    })

                }
                traverse_children(units, [unit.id])

                // Insert before we see one of our children in first spot
                var idx = -1
                unit_groups.every((group,index) => { if (!('children' in unit) || !(group[0] in unit.children)) { idx=index } })
                if (idx > 0) {
                    unit_groups.splice.apply(unit_groups, [idx-1, 0].concat(cur_groups))
                } else {
                    unit_groups = unit_groups.concat(cur_groups)
                }
            })

            console.log('unit_groups', unit_groups)

        // 1. For each rank in a unit initialize a node
        unit_groups.forEach(group => {
            group.forEach(unit => {
                ranks.filter(rank => 'units' in rank ? rank.units.filter(u => u == unit).length > 0 : false)
                    .forEach(rank => {
                        var node = {
                            "rank": rank,
                            "units": group,
                            "parentId": "",
                            "members": [],
                            get id() { return `${this.rank.id}_${this.units.map(unit => unit).join('-')}` }
                        }
                        if (!(group in model)) {
                            model[group] = []
                        }
                        model[group].push(node)
                    })
            })

        })

        console.log('Model initialized', model)

        // 2. Create relationships with `parentId`
        unit_groups.forEach((unit_group, idx) => {

            // 3. Assign parents within equal units sets.
            //    We should trust that ranks are sorted by rank
            model[unit_group].forEach((node, innerIdx, inner) => {
                if (innerIdx == 0) { return; }
                var parent = inner[innerIdx-1]
                inner[innerIdx].parentId = parent.id
            })

            // 4. Find higher ranking officers from higher units
            //    for the highest ranking officers in a unit.
            if (idx == 0) { return; } // highest unit and highest rank.
            var parent_group = model[ unit_groups[idx-1] ]
            var parent = parent_group[ parent_group.length - 1 ]
            model[unit_group][0].parentId = parent.id

        })

        console.log('Model related ', model)

        var results = cloneDeep(model);

        members.filter(member => 'rank' in member && 'units' in member)
            .forEach(member => {
                var group = unit_groups.filter(group => group.length == member.units.length
                                                        && member.units.every(unit => group.includes(unit.id) ))
                if (group.length > 1) { console.error('Found more than one unit_group match o.O') }
                else {
                    var node = results[group].find(node => node.rank.id == member.rank.id);
                    if (node) {
                        node.members.push(member)
                    }
                }
            })

        console.log('Results with members ', results)


        // Search for parent existing in results
        function search_parent(model, node, results) {

            function find_parent(model, node) {
            // what about just finding higher ranked officer in current units?!
                var result = null;
                unit_groups.forEach((group, groupIdx) => {
                    model[group].forEach((other, nodeIdx) => {
                        if (result) { return; }
                        if (other.id == node.parentId) {
                            result = {"group": groupIdx, "node": nodeIdx};
                            return;
                        }
                    })
                })
                return result;
            }
            function is_unit_descendant(unit_id, parent_id) {
                var is_descendant = false
                var parent = units.find(ounit => parent_id == ounit.id)
                if (!('children' in parent)) { return is_descendant; }
                units.find(ounit => parent_id == ounit.id).children
                    .forEach(child_id => {
                        if (is_descendant) { return is_descendant; }
                        is_descendant = child_id == unit_id ? true : is_unit_descendant(unit_id, child_id)
                    })
                return is_descendant;
            }
            if (node.parentId == "") { return; } // root node
            // Check if node.parentId
            //  1. exists,
            //  2. has a higher rank and
            //  3. shares the same units or all its units are
            //     descendendants of the parent's units.
            if (results.some(
                // look for parent with correct parentId
                parent => parent.id == node.parentId
                        // ensure our parent is a higher rank
                        && ranks.findIndex(rank => parent.rank.id == rank.id) < ranks.findIndex(rank => node.rank.id == rank.id)
                        // ensure our parent is in the correct units
                        && node.units.every((unit, idx) =>
                            // 1. If units are the same
                            unit === parent.units[idx]
                            // 2. or if each of my unit is a descendant of my parent units
                            || parent.units.every(punit => punit == unit || is_unit_descendant(unit, punit)))
                )) {
                console.log('PARENT IS CORRECT')
                return; // parent seems valid
            }

            const idx = find_parent(model, node)
            if (!(idx)) {
                console.error("We failed to find the parent o.O")
            }
            if (idx.node == 0) {
                if (idx.group == 0) {
                    console.error('Something went wrong, we are apparently the root node, but parentId is not ""?')
                    return;
                    // This only happens if the root is missing... (no members?...).
                    // This usually should not happen. What would we do without the highest
                    // ranking officer? ; )
                    // => Perhaps upgrade this node to new root in that case
                    //      setting parentId = ""?
                } else {
                    var next_nodes = model[ unit_groups[idx.group-1] ]
                    node.parentId = next_nodes[ next_nodes.length - 1 ].id
                }
            } else {
                node.parentId = model[ unit_groups[idx.group] ][ idx.node - 1 ].id
            }
            search_parent(model, node, results)
        }

      results = Object.values(results).flat().filter(node => node.members.length > 0)
      console.log('Results without empty nodes', results)
      for (let i = 0 ; i < results.length ; i++) {
          var node = results[i]
            console.log(`Searching parent for ${node.members.map(member => member.name).join(', ')}`)
            search_parent(model, node, results)
      }

        console.log('results final', results)

        return results
}
