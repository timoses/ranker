

// x[kind][version] = data
const scdata_cache = {}

export function import_sc(kind, sc_version, scdata, scdata_error, scdata_loaded_callback) {
    if (! (kind in scdata_cache)) {
        scdata_cache[kind] = {}
    }

    if (sc_version in scdata_cache[kind]) {
        scdata.value = scdata_cache[kind][sc_version]
        if (scdata_loaded_callback)
            scdata_loaded_callback();
    } else {
      import(`../assets/${kind}_${sc_version}.json`)
        .then((json) => {
          scdata_cache[kind][sc_version] = json
          scdata.value = json;
          if (scdata_loaded_callback)
              scdata_loaded_callback();
        })
        .catch((err) => {
          scdata_error.value = `Failure fetching scdata: ${err}`;
        })
    }
}
