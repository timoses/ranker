export const vmf_structure_yaml= `
# Sorted list of ranks from highest to lowest
ranks:
  - id: captain
    name: Captain
    units:
      - command
  - id: commander
    name: Commander
    units:
      - command
  - id: "lieutenant-commander"
    name: Lt. Commander
    units:
      - carrier
      - medic
  - id: "first-lieutenant"
    name: First Lieutenant
    units: []
  - id: lieutenant
    name: Lieutenant
    units:
      - marines
      - engineer
      - fighter
      - heavy
  - id: "lieutenant-junior-grade"
    name: Lieutenant Junior Grade
    units:
      - marines
      - engineer
      - fighter
  - id: "master-petty-officer"
    name: Master Petty Officer
    units: []
  - id: "chief-petty-officer"
    name: Chief Petty Officer
    units: []
  - id: "petty-officer-first-class"
    name: Petty Officer first class
    units: []
  - id: "petty-officer-first-class"
    name: Petty Officer first class
    units:
      - marines
      - engineer
      - fighter
      - heavy
  - id: "petty-officer"
    name: Petty Officer
    units:
      - carrier
      - fighter

  - id: "chief-master-sergeant"
    name: Chief Master Sergeant
    units: []
  - id: "senior-master-sergeant"
    name: Senior Master Sergeant
    units: []
  - id: "master-sergeant"
    name: Master Sergeant
    units: []
  - id: "staff-sergeant"
    name: Staff Sergeant
    units: []
  - id: sergeant
    name: Sergeant
    units:
      - marines

units:
  command:
    name: Kommandostab
    children:
      - carrier
      - medic
      - fighter
      - heavy
  carrier:
    name: "Träger-Gruppe"
    children:
      - marines
      - engineer
  medic:
    name: "Medic-Gruppe"
  fighter:
    name: Jäger-Gruppe
  heavy:
    name: "Schwere Kampfgruppe"
  engineer:
    name: Techniker
  marines:
    name: Marines
`
