import { createApp } from 'vue'
import { createRouter, createWebHashHistory } from 'vue-router'
import VueKonva from 'vue-konva';

import App from './App.vue'

import Welcome from './components/Welcome.vue'
import About from './components/About.vue'
//import VMFChart from './components/VMFChart.vue'
import Logi from './components/Logi.vue'
import Keys from './components/Keys.vue'
import Issues from './components/Issues.vue'
import Donate from './components/Donate.vue'

import '@mdi/font/css/materialdesignicons.css'
import {createVuetify} from 'vuetify'
import {
    VProgressCircular,
    VCheckbox,
    VChipGroup,
    VChip,
    VSheet,
    VSlider,
    VTextField
} from 'vuetify/components'

const routes = [
    { path: '/', component: Welcome },
    //{ path: '/chart', component: VMFChart },
    { path: '/logi', component: Logi },
    { path: '/keys', component: Keys },
    { path: '/issues', component: Issues },
    { path: '/donate', component: Donate },
    { path: '/about', component: About },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

const vuetify = createVuetify({
    components: {
        'VProgressCircular': VProgressCircular,
        'VChipGroup': VChipGroup,
        'VChip': VChip,
        'VSheet': VSheet,
        'VSlider': VSlider,
        'VTextField': VTextField,
        'VCheckbox': VCheckbox
    }
})

createApp(App)
    .use(router)
    .use(vuetify)
    .use(VueKonva)
    .mount('#app')
