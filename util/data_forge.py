#!/usr/bin/env python

import json
import logging
import math
import re
import sys

from glob import glob
from pathlib import Path

ENTITIES = dict(
    armor="Data/Libs/foundry/records/entities/scitem/characters/human/armor/pu_armor/",
    clothing="Data/Libs/foundry/records/entities/scitem/characters/human/clothing/pu_clothing/",
    carryable="Data/Libs/foundry/records/entities/scitem/characters/carryables/",
    consumable="Data/Libs/foundry/records/entities/scitem/characters/consumables/",
    ship_cooler="Data/Libs/foundry/records/entities/scitem/ships/cooler/",
    ship_powerplant="Data/Libs/foundry/records/entities/scitem/ships/powerplant/",
    ship_quantumdrive="Data/Libs/foundry/records/entities/scitem/ships/quantumdrive/",
    ship_shieldgenerator="Data/Libs/foundry/records/entities/scitem/ships/shieldgenerator/",
    ship_misc="Data/Libs/foundry/records/entities/scitem/ships/utility/mining/misc/",
    ship_weapon_mount="Data/Libs/foundry/records/entities/scitem/ships/weapon_mounts/",
    ship_weapon="Data/Libs/foundry/records/entities/scitem/ships/weapons/",
    weapon="Data/Libs/foundry/records/entities/scitem/weapons/fps_weapons/",
    gadget="Data/Libs/foundry/records/entities/scitem/weapons/gadgets/",
    magazines="Data/Libs/foundry/records/entities/scitem/weapons/magazines/",
    melee="Data/Libs/foundry/records/entities/scitem/weapons/melee/",
    throwable="Data/Libs/foundry/records/entities/scitem/weapons/throwable/",
    weapon_modifier="Data/Libs/foundry/records/entities/scitem/weapons/weapon_modifier/"
)

def get_component(data, component_name):
    if not data.get('Components'):
        return None
    components = data.get('Components')
    if component_name in components:
        return components.get(component_name)


class Turner():

    def __init__(self, path):
        self.path = path

        self._localization = None

        self._ships = []
        self._inventory_containers = []
        self._entities = []

        self._actionmaps = []
        self._action_localized = {}

    @property
    def ships(self):
        if len(self._ships) == 0:
            self.load_ships()
        return self._ships

    @property
    def entities(self):
        if len(self._entities) == 0:
            self.load_entities()
        return self._entities

    @property
    def inventory_containers(self):
        if len(self._inventory_containers) == 0:
            self.load_inventory_containers()
        return self._inventory_containers

    @property
    def actionmaps(self):
        if len(self._actionmaps) == 0:
            self.load_actionmaps()
        return self._actionmaps
    @property
    def action_localized(self):
        if len(self._action_localized) == 0:
            self.load_action_localized()
        return self._action_localized

    @property
    def localization(self):
        if self._localization == None:
            def convert(mat):
                return [mat[0], mat[2] + ' (' + mat[1] + ') '] if mat[1] is not None else [mat[0], mat[2]]
            english_file = self.path / 'Data/Localization/english/global.ini'
            regex = re.compile('(.*?)(,P)?=(.*)')
            self._localization = dict(
                    convert(regex.match(_).groups())
                    # uft-8-sig removes BOM
                    for _ in open(english_file, encoding="utf-8").read().split("\n")
                    if "=" in _
                )
        return self._localization

    def get_name(self, entity):
        name_comp = get_component(entity, 'SCItemPurchasableParams')
        if not name_comp:
            return None
        return self.localization.get(name_comp.get('@displayName')[1:]) \
            if name_comp else None

    def load_inventory_containers(self):
        for inv_container in path.glob('Data/Libs/foundry/records/inventorycontainers/ships/**/*.json'):
            icj = json.load(inv_container.open())
            self._inventory_containers.append(icj)

    def load_ships(self):
        for ship_file in path.glob('Data/Libs/foundry/records/entities/spaceships/*.json'):
            sj = json.load(ship_file.open())

            ship = dict(
                type="ship",
                guid=sj['@__ref'],
                filename=ship_file.name,
                name=self.get_name(sj)
            )

            vehicle_params = get_component(sj, 'VehicleComponentParams')
            inventory_container_id = vehicle_params.get('@inventoryContainerParams')

            # Lookup inventory container
            ic = [v for v in self.inventory_containers if v.get('@__ref') == inventory_container_id]
            ic = ic[0] if len(ic) \
                else logger.warning('Could not find inventoryContainer for ship'
                                    f'{ship["filename"]} with guid {inventory_container_id}')

            if ic:
                ic_type = ic.get('inventoryType').get('InventoryClosedContainerType')

                cargo_capacity = 0
                if 'SStandardCargoUnit' in ic_type['capacity']:
                    if '@standardCargoUnits' in ic_type['capacity']['SStandardCargoUnit']:
                        cargo_capacity = math.trunc(1000 * 1000 * float(ic_type['capacity']['SStandardCargoUnit']['@standardCargoUnits']))
                elif 'SMicroCargoUnit' in ic_type['capacity']:
                    if '@microSCU' in ic_type['capacity']['SMicroCargoUnit']:
                        cargo_capacity = math.trunc(float(ic_type['capacity']['SMicroCargoUnit']['@microSCU']))
                ship.update({'cargo_microscu': cargo_capacity})
                self._ships.append(ship)

    def load_entities(self):
        for ent_name in ENTITIES.keys():

            ent_path = path / ENTITIES.get(ent_name)

            for ent_file in ent_path.glob('**/*.json'):
                entj = json.load(ent_file.open())

                name = self.get_name(entj)
                if not name:
                    continue

                volume_comp = get_component(entj, "SAttachableComponentParams")
                if not volume_comp:
                    continue

                entity = dict(
                    name=name,
                    type=ent_name,
                    id=entj.get('@__ref'),
                    filename=ent_file.name,
                    volume_microscu=volume_comp['AttachDef']['inventoryOccupancyVolume']['SMicroCargoUnit']['@microSCU']
                )
                self._entities.append(entity)

    def load_actionmaps(self):
        default_profile = path / 'Data/Libs/Config/defaultProfile.json'
        dpj = json.load(default_profile.open())

        for s_actionmap in dpj['actionmap']:
            actionmap = {}
            actionmap['name'] = s_actionmap['@name']

            try:
                actionmap['label'] = s_actionmap['@UILabel']
                actionmap['l_english_label'] = self.localization.get(s_actionmap['@UILabel'].lstrip('@'))
            except Exception as e:
                logger.warning('Failed to interpret '
                               f'Actionmap {actionmap["name"]}. Error: {e}')
                continue

            actionmap['actions'] = []

            # If only one action, scdatatools converts it to a dict...
            # => Make a list
            if type(s_actionmap['action']) is dict:
                action_list = [s_actionmap['action']]
                s_actionmap['action'] = action_list

            for s_action in s_actionmap['action']:
                action = {}
                action['name'] = s_action['@name']
                if '@UILabel' in s_action:
                    action['label'] = s_action['@UILabel']
                    action['l_english_label'] = self.localization.get(s_action['@UILabel'].lstrip('@'))
                if '@UIDescription' in s_action:
                    action['description'] = s_action['@UIDescription']
                    action['l_english_description'] = self.localization.get(s_action['@UIDescription'].lstrip('@'))

                defaults = {}
                defaults['mouse'] = s_action['@mouse'].strip() if '@mouse' in s_action else ''
                defaults['keyboard'] = s_action['@keyboard'].strip() if '@keyboard' in s_action else ''
                defaults['gamepad'] = s_action['@gamepad'].strip() if '@gamepad' in s_action else ''
                defaults['joystick'] = s_action['@joystick'].strip() if '@joystick' in s_action else ''

                action['defaults'] = defaults

                actionmap['actions'].append(action)

            self._actionmaps.append(actionmap)

    def load_action_localized(self):
        loc = path / 'Data/Libs/Config/keybinding_localization.json'
        locj = json.load(loc.open())

        for device in locj['device']:
            device_name = device['@name']
            locs = {}
            for skey in device['Key']:
                key = {}
                key['localizationString'] = skey['@localizationString']
                key['l_english'] = self.localization.get(skey['@localizationString'].lstrip('@'))

                locs[skey['@name']] = key

            self._action_localized[device_name] = locs


if __name__ == '__main__':

    logger = logging.getLogger()

    part = sys.argv[1]
    path = Path(sys.argv[2]).absolute()

    turner = Turner(path)

    if part == "logi":
        print(json.dumps(dict(
            ships=turner.ships,
            entities=turner.entities
        )))
    elif part == "keys":
        print(json.dumps(dict(
            actionmaps=turner.actionmaps,
            localized=turner.action_localized
        )))
