#!/bin/bash

if [ -z $DEPLOY_SERVER ] ; then
    read -e -r -p "Where to deploy this to: " DEPLOY_SERVER
fi
TMPDIR=$(ssh $DEPLOY_SERVER mktemp -d)
scp -r ./dist/* $DEPLOY_SERVER:$TMPDIR
ssh $DEPLOY_SERVER sudo cp -r $TMPDIR/* /var/www/html/galactic_hike
ssh $DEPLOY_SERVER rm -rf $TMPDIR
