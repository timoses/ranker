#!/bin/bash

SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

NEXT=""

echo "<p>"
echo "This page lists changes in Actionmaps of Star Citizen's key binding table."
echo "</br><span style=\"color: red;\">Red color means an Action was removed from the Actionmap.</span>"
echo "</br><span style=\"color: green;\">Green color means an Action was added to the Actionmap.</span>"
echo "</p>"

for VERSION in $(ls -r $SCRIPTPATH/../src/assets/keys_* | sed '$d') ; do
    VERSION=$(basename $VERSION | cut -d'_' -f2 | cut -d'.' -f1-3)
    echo "<a href=\"#$VERSION\">$VERSION</a></br>"
done

echo "<ul>"
for PREV in $(ls -r $SCRIPTPATH/../src/assets/keys_*) ; do
    if [ -z $NEXT ] ; then
        NEXT=$PREV
        continue
    fi

    VERSION=$(basename $NEXT | cut -d'_' -f2 | cut -d'.' -f1-3)
    VERSION_PREV=$(basename $PREV | cut -d'_' -f2 | cut -d'.' -f1-3)
    echo "    <li id="$VERSION">$VERSION_PREV =&gt; <b>$VERSION</b></li>"

    echo "    <ul>"
    DIFF=$(diff <(cat $PREV | jq '.actionmaps[].name') <(cat $NEXT | jq '.actionmaps[].name'))

    if $(echo "$DIFF" | grep -qs '^<') ; then
        echo -e "$DIFF" | grep '^<' | sed -e 's/"//g' -e 's/< /<li>Actionmap <b>/g' -e 's/$/<\/b>  <b style="color: red;">removed<\/b><\/li>/' -e 's/^/        /'
    fi

    AMS=$(cat $NEXT | jq '.actionmaps[].name')

    for AM in $AMS ; do
        JQ_ACTIONS=".actionmaps | map(select(.name == $AM)) | first | .actions[].name"
        AM="$(echo $AM | sed 's/"//g')"
        if ! $(grep -qs "$AM" $PREV) ; then
            echo "        <li>Actionmap <b>$AM</b> <b style=\"color: green;\">added</b>"
        else
            DIFF=$(diff <(cat $PREV | jq "$JQ_ACTIONS" 2> /dev/null) <(cat $NEXT | jq "$JQ_ACTIONS" 2> /dev/null))

            if [ -n "$DIFF" ] ; then
                echo "        <li>Actionmap <b>$AM</b> <span style=\"color: orange;\">changed</span></li>"
                echo "        <ul>"
                if $(echo "$DIFF" | grep -qs '^<') ; then
                    echo -e "$DIFF" | grep '^<' | sed -e 's/"//g' -e 's/< /<li style="color: red;">/g' -e 's/$/<\/li>/' -e 's/^/                /'
                fi
                if $(echo "$DIFF" | grep -qs '^>') ; then
                    echo -e "$DIFF" | grep '^>' | sed -e 's/"//g' -e 's/> /<li style="color: green;">/g' -e 's/$/<\/li>/' -e 's/^/                /'
                fi
                echo "        </ul>"
            fi

        fi

    done

    echo "    </ul>"


    NEXT=$PREV
done
echo "</ul>"

