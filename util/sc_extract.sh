#!/bin/bash

SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

if [ -z $UNP4K_PATH ] ; then
    read -p "Path to unp4k (UNP4K_PATH): " UNP4K_PATH
fi
if [ -z $SC_VERSION ] ; then
    read -p "SC Version of data.p4k (e.g. '3.17.2')(SC_VERSION): " SC_VERSION
fi

EXTRACT_PATH=$SCRIPTPATH/extracted/$SC_VERSION

if [ -d $EXTRACT_PATH ] ; then
    read -n1 -p "Extract path already exists ($EXTRACT_PATH)! Overwrite and extract again? (y/n) " EXTRACT_PATH_OVERWRITE
    echo
fi

if [ ! -d $EXTRACT_PATH ] || [[ $EXTRACT_PATH_OVERWRITE == "y" ]] ; then

    rm -rf $EXTRACT_PATH
    mkdir -p $EXTRACT_PATH

    echo "Extracting version '$SC_VERSION' from '$PATH_TO_DATAP4K' into '$EXTRACT_PATH'"


    #######
    # Logi
    # TODO: With 3.20 this was falsely encoded:
    #   Fixed with: iconv -f ISO-8859-1 -t utf-8 -o global.ini extracted/Data/Localization/english/global.ini
    #'libs/foundry/records/entities/spaceships/*'
    #'libs/foundry/records/entities/scitem/*'
    #'libs/foundry/records/inventorycontainers/ships/*'
    #######
    # Keys
    #"**/Libs/Config/defaultProfile.xml"
    #"**/Libs/Config/keybinding_localization.xml"

    cat << EOF
Run the following in $UNP4K_PATH (https://github.com/dolkensp/unp4k):
    .\unp4k.exe C:\games_rsi\StarCitizen\LIVE\Data.p4k Data/Localization/english/global.ini

    .\unp4k.exe C:\games_rsi\StarCitizen\LIVE\Data.p4k Data/Libs/Config/defaultProfile.xml
    .\unforge.exe Data/Libs/Config/defaultProfile.xml
    .\unp4k.exe C:\games_rsi\StarCitizen\LIVE\Data.p4k Data/Libs/Config/keybinding_localization.xml
    .\unforge.exe Data/Libs/Config/keybinding_localization.xml

    .\unforge.exe Data\Game2.dcb
EOF
    read -p "Press any key to continue" </dev/tty

    echo "Copying files for Logi ..."
    for path in Data/Localization/english/global.ini Data/Libs/foundry/records/entities/spaceships Data/Libs/foundry/records/entities/scitem Data/Libs/foundry/records/inventorycontainers/ships ; do
        echo "  Copying $path ..."
        mkdir -p $EXTRACT_PATH/$(dirname $path)
        rsync -r --info=progress2 $UNP4K_PATH/$path $EXTRACT_PATH/$(dirname $path)
    done

    echo "Copying files for Keys ..."
    for path in Data/Libs/Config/defaultProfile.xml Data/Libs/Config/keybinding_localization.xml ; do
        echo "  Copying $path ..."
        mkdir -p $EXTRACT_PATH/$(dirname $path)
        rsync -r --info=progress2 $UNP4K_PATH/$path $EXTRACT_PATH/$(dirname $path)
    done


    iconv -f ISO-8859-1 -t utf-8 -o /tmp/sc_global.ini $EXTRACT_PATH/Data/Localization/english/global.ini
    mv $EXTRACT_PATH/Data/Localization/english/global.ini{,.backup}
    mv /tmp/sc_global.ini $EXTRACT_PATH/Data/Localization/english/global.ini


    XML_PATH="$EXTRACT_PATH/Data/Libs"
    TOTAL=$(find $XML_PATH -name '*.xml' | wc -l)
    i=0
    while IFS= read -r file ; do
        while [ `jobs | wc -l` -ge 50 ] ; do
            sleep 1
        done
        xq "to_entries | first | .value" < "$file" > "${file%.xml}.json" &
        i=$((i+1))
        echo -ne "Converting XML to JSON ... $i / $TOTAL \r"
    done <<< $(find $XML_PATH -name '*xml')
    while [ `jobs | wc -l` -ge 1 ] ; do
        sleep 1
        jobs
    done
    echo -ne "Done\n"

fi


echo "###### Loading Logi data"
$SCRIPTPATH/data_forge.py logi $EXTRACT_PATH > $SCRIPTPATH/../src/assets/logi_${SC_VERSION}.json


echo "###### Loading Keys data"
$SCRIPTPATH/data_forge.py keys $EXTRACT_PATH > $SCRIPTPATH/../src/assets/keys_${SC_VERSION}.json
